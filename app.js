const express = require('express')
require('dotenv').config()
const request = require('request')
const app = express()
const port = process.env?.PORT || 8080 
const CLIENT_IP = process.env?.CLIENT_IP || 8080 
app.get("/", (req, res) => {
  res.send("Users Shown");
});

app.get("/delete", (req, res) => {
  res.send("Delete User");
});

app.get("/update", (req, res) => {
  res.send("Update User");
});

app.get("/insert", (req, res) => {
  res.send("Insert User");
});

app.get("/1", (req, res) => {
  res.send("Users Shown 1");
});

app.get("/1/delete", (req, res) => {
  res.send("Delete User 1");
});

app.get("/1/update", (req, res) => {
  res.send("Update User 1");
});

app.get("/1/insert", (req, res) => {
  res.send("Insert User 1");
});


app.get("/check", (req, res) => {
  request({
    url: `http://${CLIENT_IP}:8081/1`,
    method: 'get'
  }, function(error, response, body){
    res.send(response);
  });
});


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})